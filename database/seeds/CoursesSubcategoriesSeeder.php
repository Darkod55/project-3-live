<?php

use Illuminate\Database\Seeder;
use App\CoursesSubcategories;

class CoursesSubcategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 350; $i++) {
            CoursesSubcategories::create([
                'course_id' => $faker->numberBetween(1,200),
                'subcategory_id' => $faker->numberBetween(1,119),
            ]);
        }
    }
}
