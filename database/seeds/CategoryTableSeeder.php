<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')
        ->insert([
            ['name' => 'Programming', 'slug' => "programming", 'icon' => "<i class='fal fa-code'></i>", 'search_word' => 'language', 'exm_tools' => "Python, Javascript"],
            ['name' => 'Data Science', 'slug' => "data-science", 'icon' => "<i class='fal fa-atom-alt'></i>", 'search_word' => 'technology', 'exm_tools' => "ML, Data Science"],
            ['name' => 'DevOps', 'slug' => "devops", 'icon' => "<i class='fal fa-infinity'></i>", 'search_word' => 'technology', 'exm_tools' => "AWS, Docker"],
            ['name' => 'Design', 'slug' => "design", 'icon' => "<i class='fal fa-paint-brush-alt'></i>", 'search_word' => 'design tools', 'exm_tools' => "Photoshop, Sketch"]
        ]);
    }
}
