<?php

use Illuminate\Database\Seeder;

class MediumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media')
        ->insert([
            ['medium' => 'Book'],
            ['medium' => 'Video'],
        ]);
    }
}
