<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')
        ->insert([
            ['language' => 'English'],
            ['language' => 'German'],
            ['language' => 'French'],
            ['language' => 'Macedonian'],
        ]);
    }
}
