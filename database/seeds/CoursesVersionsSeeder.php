<?php

use Illuminate\Database\Seeder;
use App\Courses_Versions;
class CoursesVersionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 350; $i++) {
            DB::table('courses_versions')->insert([
                'course_id' => $faker->numberBetween(1,200),
                'version_id' => $faker->numberBetween(1,155),
            ]);
        }
    }
}
