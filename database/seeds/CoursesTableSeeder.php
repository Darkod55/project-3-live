<?php

use Illuminate\Database\Seeder;
use App\Subcategory;
use App\Course;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('courses')
        // ->insert([
        //     ['name' => 'Python for Everybody Specialization', 'language_id' => 1, 'level_id' => 1, 'medium_id' => 1, "type_id" => 1, "user_id" => 1, "link" => "https://www.coursera.org/specializations/python?ranMID=40328&ranEAID=jU79Zysihs4&ranSiteID=jU79Zysihs4-47bqHJjcvwYikVvEXlqvzg&siteID=jU79Zysihs4-47bqHJjcvwYikVvEXlqvzg&utm_content=10&utm_medium=partners&utm_source=linkshare&utm_campaign=jU79Zysihs4", "views" => 12],
        //     ['name' => 'Learning Python with PyCharm', 'language_id' => 1, 'level_id' => 2, 'medium_id' => 1, "type_id" => 1, "user_id" => 1, "link" => "https://www.linkedin.com/learning/learning-python-with-pycharm?src=aff-lilpar&veh=aff_src.aff-lilpar_c.partners_pkw.1419154_plc.Hackr.io_pcrid.449670_learning&trk=aff_src.aff-lilpar_c.partners_pkw.1419154_plc.Hackr.io_pcrid.449670_learning&clickid=U3iXPe176xyOUWRwUx0Mo3EUUknS%3A2yIPVq5RM0&irgwc=1", "views" => 3],
        //     ['name' => 'Complete Python Bootcamp: Go from zero to hero in Python 3', 'language_id' => 1, 'level_id' => 2, 'medium_id' => 1, "type_id" => 2, "user_id" => 1, "link" => "https://www.udemy.com/course/complete-python-bootcamp/?ranMID=39197&ranEAID=jU79Zysihs4&ranSiteID=jU79Zysihs4-bGKfcxyf.EVKD8WFn0gpBQ&LSNPUBID=jU79Zysihs4", "views" => 13],
        //     ['name' => 'Java Programming Masterclass for Software Developers', 'language_id' => 2, 'level_id' => 1, 'medium_id' => 2, "type_id" => 2, "user_id" => 1, "link" => "https://www.udemy.com/course/java-the-complete-java-developer-course/?LSNPUBID=jU79Zysihs4&ranEAID=jU79Zysihs4&ranMID=39197&ranSiteID=jU79Zysihs4-aMZZPgLfnsY2ZRsuqISdWQ", "views" => 7],
        //     ['name' => 'Seriously Good Software', 'language_id' => 1, 'level_id' => 3, 'medium_id' => 2, "type_id" => 1, "user_id" => 1, "link" => "https://www.manning.com/books/seriously-good-software?a_aid=hackrio", "views" => 7],
        //     ['name' => 'Head First Java', 'language_id' => 1, 'level_id' => 2, 'medium_id' => 2, "type_id" => 2, "user_id" => 1, "link" => "https://www.amazon.it/dp/0596009208?tag=hackr069-21", "views" => 9],
        // ]);
        $faker = Faker\Factory::create();
        $subcategory = Subcategory::all();
        
        for($i = 0; $i < 200; $i++) {
            $subId = $subcategory->random()->id;

            Course::create([
                'name' => $subcategory->find($subId)->name . " - " . $faker->realText($faker->numberBetween(10, 25)),
                'link' => $faker->url,
                'type_id' => $faker->numberBetween(1,2),
                'medium_id' => $faker->numberBetween(1,2),
                'level_id' => $faker->numberBetween(1,3),
                'language_id' => $faker->numberBetween(1,4),
                'user_id' => 1,
                'views' => 0,
                'status' => $faker->numberBetween(0,1)
            ]);
        }
    }
}
