<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->after('link');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('type_id')->after('link');
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
            $table->unsignedBigInteger('medium_id')->after('link');
            $table->foreign('medium_id')->references('id')->on('media')->onDelete('cascade');
            $table->unsignedBigInteger('level_id')->after('link');
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->unsignedBigInteger('language_id')->after('link');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn(['user_id']);
            $table->dropForeign(['type_id']);
            $table->dropColumn(['type_id']);
            $table->dropForeign(['medium_id']);
            $table->dropColumn(['medium_id']);
            $table->dropForeign(['level_id']);
            $table->dropColumn(['level_id']);
            $table->dropForeign(['language_id']);
            $table->dropColumn(['language_id']);
        });
    }
}
