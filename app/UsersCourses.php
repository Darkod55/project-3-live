<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersCourses extends Model
{
    public function usersvotes()
    {
        return $this->belongsTo('Course');
    }

    public function coursesvotes()
    {
        return $this->belongsTo('User');
    }
}
