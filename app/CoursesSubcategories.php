<?php

namespace App;
use App\Subcategory;
use App\Course;

use Illuminate\Database\Eloquent\Model;

class CoursesSubcategories extends Model
{
    public function courses()
    {
        return $this->belongsTo('Course');
    }

    public function subcategories()
    {
        return $this->belongsTo('Subcategory');
    }
}
