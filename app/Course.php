<?php

namespace App;

use App\Filters\CoursesFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Course extends Model
{

    protected $with = ['types', 'mediums', 'levels', 'languages', 'users', 'versions', 'subcategories', 'coursesvotes'];

    public function types() {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function mediums() {
        return $this->belongsTo(Medium::class, 'medium_id');
    }

    public function levels() {
        return $this->belongsTo(Level::class, 'level_id');
    }

    public function languages() {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }
 
    public function versions() {
        return $this->belongsToMany(Version::class, 'courses_versions');
    }

    public function subcategories() {
        return $this->belongsToMany(Subcategory::class, 'courses_subcategories');
    }

    public function coursesvotes() {
        return $this->belongsToMany(User::class, 'users_courses');
    }

    public function scopeFilter(Builder $builder, $request)
    {
        return (new CoursesFilter($request))->filter($builder);
    }
}
