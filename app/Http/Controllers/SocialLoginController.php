<?php

namespace App\Http\Controllers;
use Socialite;
use App\User;
use App\Events\UserRegistered;

use Illuminate\Http\Request;

class SocialLoginController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $userSocial = Socialite::driver($provider)->stateless()->user();
        if(User::where('email', '=', $userSocial->email)->count() > 0) {
            $users = User::where(['email' => $userSocial->getEmail()])->first();
            \Auth::login($users);
            return redirect()->route('Index');
        } else {
            $user = new User();
            $user->name = $userSocial->name;
            $user->email = $userSocial->email;
            $user->role_id = 2;
            $user->api_token = \Str::random(80);
            $user->save();

            event(new UserRegistered($user));

            $users = User::where(['email' => $user->email])->first();
            \Auth::login($users);

            return redirect()->route('Index');
        }
        //dd($userSocial);
    }
}
