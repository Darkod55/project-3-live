<?php

namespace App\Http\Controllers;
use App\Category;
use App\Subcategory;
use App\Course;
use App\User;
use App\CoursesSubcategories;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {    
        if(\Request('slug') == 'testfilter') {
            // dd(\Request('slug'));
        return redirect()->route('testfilter');
        }

        if(\Request('slug') == 'dashboard') {
            // dd(\Request('slug'));
        return redirect()->route('dashboard');
        } 
        
        if(empty(\Request('slug'))) {
            $category = Category::with('subcategories')->get();
            $slug = "/";
            return view('index', compact('category', 'slug'));
        } else if(strpos(\Request('slug'), 'courses') !== false) {
            return redirect()->route('courses');
        }  else if(\Request('slug') != 'dashboard') {
            $category = Category::with('subcategories')->where('slug', \Request('slug'))->first();
            $slug = $category->slug;
            return view('index', compact('category', 'slug'));
        }
        
    }

    public function showSubcategories() {
        if(empty(\Request('slug'))) {
            $category = Category::with('subcategories')->get();
        } else {
            $category = Category::with('subcategories')->where('slug', \Request('slug'))->first();
        }
        return response()->json($category);
    }
    
    public function search(Request $request) {
        if($request->slug == "/") {
            $subcategories = Subcategory::where('name', 'LIKE', '%'.$request->get('search').'%')->get();;
        } else {
            $category = Category::where('slug', trim($request->slug, "/"))->first()->id;
            $subcategories = Subcategory::with('categories')->where('category_id', $category)
            ->where('name', 'LIKE', '%'.$request->get('search').'%')
            ->get();
        }
        
        return response()->json($subcategories);
    }

    public function testFilter(Request $request) {
        $subcategory = Subcategory::with('courses')->where('slug', 'python')->first();

        //Put and count Type
        $arrfortype = [];
        foreach($subcategory->courses as $type) {
            $arrfortype[] = $type->types['id'];
        }
        $countedType = array_count_values($arrfortype);
        $finalType = [];
        foreach($countedType as $t => $v) {
            $type = \App\Type::where('id', $t)->first();
            $finalType[] = ['id' => $t, 'type' => $type->type, 'number' => $v];
        }
        array_multisort(array_column($finalType, 'id'), SORT_ASC, $finalType);

        //Put and count Medium
        $arrformedium = [];
        foreach($subcategory->courses as $medium) {
            $arrformedium[] = $medium->mediums['id'];
        }
        $countedMedium = array_count_values($arrformedium);
        $finalMedium = [];
        foreach($countedMedium as $t => $v) {
            $medium = \App\Medium::where('id', $t)->first();
            $finalMedium[] = ['id' => $t, 'medium' => $medium->medium, 'number' => $v];
        }
        array_multisort(array_column($finalMedium, 'id'), SORT_ASC, $finalMedium);

        //Put and count Level
        $arrforlevel = [];
        foreach($subcategory->courses as $level) {
            $arrforlevel[] = $level->levels['id'];
        }
        $countedLevel = array_count_values($arrforlevel);
        $finalLevel = [];
        foreach($countedLevel as $t => $v) {
            $level = \App\Level::where('id', $t)->first();
            $finalLevel[] = ['id' => $t, 'level' => $level->level, 'number' => $v];
        }
        array_multisort(array_column($finalLevel, 'id'), SORT_ASC, $finalLevel);

        //Put and count Language
        $arrforlanguage = [];
        foreach($subcategory->courses as $language) {
            $arrforlanguage[] = $language->languages['id'];
        }
        $countedLanguage = array_count_values($arrforlanguage);
        $finalLanguage = [];
        foreach($countedLanguage as $t => $v) {
            $language = \App\Language::where('id', $t)->first();
            $finalLanguage[] = ['id' => $t, 'language' => $language->language, 'number' => $v];
        }
        array_multisort(array_column($finalLanguage, 'id'), SORT_ASC, $finalLanguage);

        // dd($finalLanguage);
        $subcategory = $subcategory->courses()->filter($request)->get();
        return view('testfilter', ['subcategory' => $subcategory, 'finalType' => $finalType, 'finalMedium' => $finalMedium, 'finalLevel' => $finalLevel, 'finalLanguage' => $finalLanguage]);
    }

}
