<?php

namespace App\Http\Controllers;
use App\Category;
use App\Subcategory;
use App\Course;
use App\User;
use App\CoursesSubcategories;
use App\Type;
use App\Medium;
use App\Level;
use App\Language;
use Carbon\Carbon;

use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function courses(Request $request) {
        $subcategory = Subcategory::with('courses', 'categories')->where('slug', $request->slug)->first(); 
        // $courses = Course::inRandomOrder()->limit(6)->get();
        // dd($subcategory->courses);
        return view('courses', ['subcategory' => $subcategory]);
    }
    
    public function listCourses(Request $request) {
        $subcategory = Subcategory::with('courses', 'categories')->where('slug', $request->slug)->first();
        $related = Subcategory::where('category_id', $subcategory->categories['id'])->inRandomOrder()->limit(6)->get();
        
            $allcourses = $subcategory->courses()->filter($request)->get();
            $courses = [];
            foreach($allcourses as $course) {
                if($course->status != 0) {
                    $courses[] = $course;
                 }
            }

        //Put and count Type
        $arrfortype = [];
        foreach($courses as $type) {
            $arrfortype[] = $type->types['id'];
        }
        $countedType = array_count_values($arrfortype);
        $finalType = [];
        foreach($countedType as $t => $v) {
            $type = \App\Type::where('id', $t)->first();
            $finalType[] = ['id' => $t, 'type' => $type->type, 'number' => $v];
        }
        array_multisort(array_column($finalType, 'id'), SORT_ASC, $finalType);

        //Put and count Medium
        $arrformedium = [];
        foreach($courses as $medium) {
            $arrformedium[] = $medium->mediums['id'];
        }
        $countedMedium = array_count_values($arrformedium);
        $finalMedium = [];
        foreach($countedMedium as $t => $v) {
            $medium = \App\Medium::where('id', $t)->first();
            $finalMedium[] = ['id' => $t, 'medium' => $medium->medium, 'number' => $v];
        }
        array_multisort(array_column($finalMedium, 'id'), SORT_ASC, $finalMedium);

        //Put and count Level
        $arrforlevel = [];
        foreach($courses as $level) {
            $arrforlevel[] = $level->levels['id'];
        }
        $countedLevel = array_count_values($arrforlevel);
        $finalLevel = [];
        foreach($countedLevel as $t => $v) {
            $level = \App\Level::where('id', $t)->first();
            $finalLevel[] = ['id' => $t, 'level' => $level->level, 'number' => $v];
        }
        array_multisort(array_column($finalLevel, 'id'), SORT_ASC, $finalLevel);

        //Put and count Language
        $arrforlanguage = [];
        foreach($courses as $language) {
            $arrforlanguage[] = $language->languages['id'];
        }
        $countedLanguage = array_count_values($arrforlanguage);
        $finalLanguage = [];
        foreach($countedLanguage as $t => $v) {
            $language = \App\Language::where('id', $t)->first();
            $finalLanguage[] = ['id' => $t, 'language' => $language->language, 'number' => $v];
        }
        array_multisort(array_column($finalLanguage, 'id'), SORT_ASC, $finalLanguage);

        //Put and count Versions
        $arrforversion = [];
        foreach($courses as $forversion) {
            foreach($forversion->versions as $version) {
                $arrforversion[] = $version->id;
            }
        }
        $countedVersions = array_count_values($arrforversion);
        $finalVersions = [];
        foreach($countedVersions as $t => $v) {
            $version = \App\Version::where('id', $t)->first();
            $finalVersions[] = ['id' => $t, 'version' => $version->name, 'number' => $v];
        }
        array_multisort(array_column($finalVersions, 'id'), SORT_ASC, $finalVersions);

        return response()->json(['subcategory' => $subcategory, 'courses' => $courses, 'finalVersions' => $finalVersions, 'finalType' => $finalType, 'finalMedium' => $finalMedium, 'finalLevel' => $finalLevel, 'finalLanguage' => $finalLanguage, 'related' => $related]);
    }

    public function votes($id, Request $request) {
        if($request->forvote == "notvoted") {
            Course::where('id', $id)->increment('views', 1);
            $courses = Course::where('id', $id)->first();
            $courses->coursesvotes()->attach(\Auth::user()->id);
        } else if($request->forvote == "voted") {
            Course::where('id', $id)->decrement('views', 1);
            $courses = Course::where('id', $id)->first();
            $courses->coursesvotes()->detach(\Auth::user()->id);
        }
        
        // dd($courses);
        return response()->json($courses);
    }

    public function storeCourse(Request $request) {
        $course = new Course();
        $course->name = $request->get('name');
        $course->link = $request->get('link');
        $course->type_id = $request->get('type');
        $course->medium_id = $request->get('medium');
        $course->level_id = $request->get('level');
        $course->language_id = $request->get('language');
        $course->user_id = \Auth::user()->id;
        $course->views = 0;
        $course->status = 0;
        $course->created_at = Carbon::now();
        $course->save();

        foreach($request->versions as $version) {
            $course->versions()->attach($version);
        }
        foreach($request->subcategories as $subcategory) {
            $course->subcategories()->attach($subcategory);
        }

        return response()->json(['message' => "Course successfully submitted. You will be contacted as soon as your course is approved!"]);
    }

    public function sortByFunction(Request $request) {
        $subcategories = Subcategory::with('courses')->where('id', $request->subcategory_id)->first();
        $selectcourse = CoursesSubcategories::where('subcategory_id', $request->subcategory_id)->get();
        $courses = [];
        foreach($selectcourse as $t) {
            $courses[] = Course::where('id', $t->course_id)->first();
        }
        $param = array_column($courses, $request->testitem);
        array_multisort($param, SORT_DESC, $courses);
        //dd($subcategories);
        return response()->json($courses);
    }

    public function filterOptions(Request $request) {
        // dd($request);
        $subcategory = Subcategory::with('courses')->where('slug', $request->slug)->first();
        $allcourses = $subcategory->courses()->filter($request)->get();
        $courses = [];
        foreach($allcourses as $course) {
            if($course->status != 0) {
                $courses[] = $course;
                }
        }

        //Put and count Type
        $arrfortype = [];
        foreach($courses as $type) {
            $arrfortype[] = $type->types['id'];
        }
        $countedType = array_count_values($arrfortype);
        $finalType = [];
        foreach($countedType as $t => $v) {
            $type = \App\Type::where('id', $t)->first();
            $finalType[] = ['id' => $t, 'type' => $type->type, 'number' => $v];
        }
        array_multisort(array_column($finalType, 'id'), SORT_ASC, $finalType);

        //Put and count Medium
        $arrformedium = [];
        foreach($courses as $medium) {
            $arrformedium[] = $medium->mediums['id'];
        }
        $countedMedium = array_count_values($arrformedium);
        $finalMedium = [];
        foreach($countedMedium as $t => $v) {
            $medium = \App\Medium::where('id', $t)->first();
            $finalMedium[] = ['id' => $t, 'medium' => $medium->medium, 'number' => $v];
        }
        array_multisort(array_column($finalMedium, 'id'), SORT_ASC, $finalMedium);

        //Put and count Level
        $arrforlevel = [];
        foreach($courses as $level) {
            $arrforlevel[] = $level->levels['id'];
        }
        $countedLevel = array_count_values($arrforlevel);
        $finalLevel = [];
        foreach($countedLevel as $t => $v) {
            $level = \App\Level::where('id', $t)->first();
            $finalLevel[] = ['id' => $t, 'level' => $level->level, 'number' => $v];
        }
        array_multisort(array_column($finalLevel, 'id'), SORT_ASC, $finalLevel);

        //Put and count Language
        $arrforlanguage = [];
        foreach($courses as $language) {
            $arrforlanguage[] = $language->languages['id'];
        }
        $countedLanguage = array_count_values($arrforlanguage);
        $finalLanguage = [];
        foreach($countedLanguage as $t => $v) {
            $language = \App\Language::where('id', $t)->first();
            $finalLanguage[] = ['id' => $t, 'language' => $language->language, 'number' => $v];
        }
        array_multisort(array_column($finalLanguage, 'id'), SORT_ASC, $finalLanguage);


        //Put and count Versions
        $arrforversion = [];
        foreach($courses as $forversion) {
            foreach($forversion->versions as $version) {
                $arrforversion[] = $version->id;
            }
        }
        $countedVersions = array_count_values($arrforversion);
        $finalVersions = [];
        foreach($countedVersions as $t => $v) {
            $version = \App\Version::where('id', $t)->first();
            $finalVersions[] = ['id' => $t, 'version' => $version->name, 'number' => $v];
        }
        array_multisort(array_column($finalVersions, 'id'), SORT_ASC, $finalVersions);
        
        
        return response()->json(['subcategory' => $subcategory, 'courses' => $courses, 'finalVersions' => $finalVersions, 'finalType' => $finalType, 'finalMedium' => $finalMedium, 'finalLevel' => $finalLevel, 'finalLanguage' => $finalLanguage]);
    
    }
}
