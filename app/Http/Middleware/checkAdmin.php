<?php

namespace App\Http\Middleware;

use Closure;

class checkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check() && \Auth::user()->role_id != 1) {
            return redirect()->route('Index');
        } else if(!\Auth::check()) {
            return redirect()->route('Index');
        }
        return $next($request);
    }
}
