<?php

namespace App\Filters;

use App\Filters\AbstractFilter;
use Illuminate\Database\Eloquent\Builder;

class CoursesFilter extends AbstractFilter
{
    protected $filters = [
        'type_id' => TypeFilter::class,
        'medium_id' => MediumFilter::class,
        'level_id' => LevelFilter::class,
        'language_id' => LanguageFilter::class,
        'version_id' => VersionFilter::class,
    ];
}