<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <form id="testform">
            <select onchange='testform.submit()' name="type_id">
                <option value="">Select Type</option>
                @foreach($subcategory as $course)
                    <option value="{{ $course->types['id'] }}">{{ $course->types['type'] }}</option>
                @endforeach
            </select>
            <select onchange='testform.submit()' name="medium_id">
                <option value="">Select Medium</option>
                @foreach($subcategory as $course)
                    <option value="{{ $course->mediums['id'] }}">{{ $course->mediums['medium'] }}</option>
                @endforeach
            </select>
            <select onchange='testform.submit()' name="level_id">
                <option value="">Select Level</option>
                @foreach($subcategory as $course)
                    <option value="{{ $course->levels['id'] }}">{{ $course->levels['level'] }}</option>
                @endforeach
            </select>
            <select onchange='testform.submit()' name="language_id">
                <option value="">Select Language</option>
                @foreach($subcategory as $course)
                    <option value="{{ $course->language['id'] }}">{{ $course->languages['language'] }}</option>
                @endforeach
            </select>
            <button type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            @foreach($subcategory as $course) 
                <h4>{{ $course->name }}</h4>
                <p><span>{{ $course->types['type'] }}</span>
                    <span>{{ $course->mediums['medium'] }}</span>
                    <span>{{ $course->levels['level'] }}</span>
                    <span>{{ $course->languages['language'] }}</span>
                </p>
                <hr>
            @endforeach
        </div>
    </div>
</div>