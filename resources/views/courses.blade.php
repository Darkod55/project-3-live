@extends('layouts.master')

@section('page-title')
    Courses - {{ $subcategory->name }}
@endsection

@section('customscripts')
<script src="{{ asset('assets/js/courses.js') }}"></script>
@endsection
@section('content')
<div class="main-container">
    <section class="breadcrumbs py-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10 breadcrumbs-line">

                </div>
            </div>
        </div>
    </section>
    <section class="more-info py-3">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row logo-sub-section">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3">
        <div class="container">
            <div class="row">
                <div class="col filter-tags">

                </div>
            </div>
        </div>
    </section>
    <section class="courses py-3">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3 sidebar rounded border py-3 bg-color">
                            <p><strong>Filter Courses</strong></p>
                            <hr>
                            <div class="fortype divborder">
                                <p><strong>Type of Course</strong></p>
                                <div class="typeFilters">

                                </div>
                            </div>
                            <div class="formedium divborder">
                                <p><strong>Medium</strong></p>
                                <div class="mediumFilters">
  
                                </div>
                            </div>
                            <div class="forlevel divborder">
                                <p><strong>Level</strong></p>
                                <div class="levelFilters">

                                </div>
                            </div>
                            <div class="forversion">
                                <p><strong>Version</strong></p>
                                <div class="versionFilters">

                                </div>
                            </div>
                            <div class="forlanguage divborder">
                                <p><strong>Language</strong></p>
                                <div class="languageFilters">

                                </div>
                            </div>
                        </div> 
                        <div class="col-md-8 ml-5 border rounded py-3">
                            <div class="top-courses d-flex justify-content-between">

                            </div>
                            <hr class="my-0">
                            <div class="forappend-courses">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3">

                        </div> 
                        <div class="col-md-8 ml-5 border rounded p-5">
                            <h4>You might also be interested in:</h4>
                            <div class="row forappend-related d-flex justify-content-between">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection