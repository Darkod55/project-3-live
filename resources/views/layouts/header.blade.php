<header>
<nav class="navbar navbar-expand px-5">
    <div class="w-100">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-7 d-flex justify-content-start align-items-center">
                <a class="navbar-brand" href="/">
                    <img src="{{ asset('assets/images/brainster.png') }}" class="logo" />
                </a>
                <div class="nav-categories ml-5">
                    <ul class="nav list-inline justify-content-center align-items-center mx-lg-auto mb-5 mb-lg-0">
                        @php
                            $categories = \App\Category::all();
                        @endphp

                        @foreach($categories as $category)
                            <li class="list-inline-item {{ (\Request('slug') == $category->slug) ? 'active' : ($category->slug == '/') ? 'active' : '' }}"><a href="/{{ $category->slug }}" class="nav-link">{!! $category->icon !!} {{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-auto d-lg-none for-index">
                <button id="hamburger-menu" class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <div id="navbarMenu" class="col-5 collapse navbar-collapse justify-content-end">
                <ul class="nav list-inline">
                    <li class="list-inline-item"><a href="" class="nav-link tutorial-submit" data-toggle="modal" data-target="@if(\Auth::check()) #SubmitTool @endif"><i class="fal fa-plus"></i> Submit a tutorial</a></li>
                    @if(Auth::check()) 
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Welcome {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right logout-option" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @else
                    <li class="list-inline-item"><a href="" class="nav-link" data-toggle="modal" data-target="#registerModal">Sign In / Sign Up</a></li>
                    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content p-3">
                                <div class="modal-header flex-column pb-0">
                                    <h3 class="modal-title text-center w-100" id="registerModalTitle">Welcome to Brainster Tools</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                    <p class="w-100 text-center para-text">Signup to submit and upvote tutorials, follow topics, and more.</p>
                                    <p class="w-100 text-center para-text mb-0">CONTINUE WITH</p>
                                </div>
                                <div class="modal-body text-center p-0">
                                    <div class="social-buttons pt-2">
                                        <div class="loginGoogle">
                                            <form action="{{ route('social.login', 'google') }}">
                                                <button class="login-buttons btn" type="submit"><img src="{{ asset('assets/images/google-signin-dark.png') }}" /></button>
                                            </form>
                                        </div>
                                        <div class="loginNext d-flex mt-2">
                                            <form action="{{ route('social.login', 'facebook') }}">
                                                <button type="submit" class="btn btn-default btn-facebook"><i class="fab fa-facebook-square"></i> Facebook</button>
                                            </form>
                                            <form action="{{ route('social.login', 'github') }}">
                                                <button type="submit" class="btn btn-default btn-github"><i class="fab fa-github-square"></i> Github</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="lines d-flex justify-content-center align-items-center pt-3">
                                        <hr class="w-100">
                                        <p class="px-3 m-0">OR</p>
                                        <hr class="w-100">
                                    </div>
                                    <div class="forma p-3">
                                        <form method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text addon"><i class='fas fa-user'></i></span>
                                                        </div>
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Full Name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                                    </div>
                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text addon"><i class="fas fa-envelope"></i></span>
                                                        </div>
                                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email">
                                                    </div>
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text addon"><i class="fas fa-lock-alt"></i></span>
                                                        </div>
                                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">
                                                    </div>
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text addon"><i class="fas fa-lock-alt"></i></span>
                                                        </div>
                                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col">
                                                    <button type="submit" class="btn bg-red btn-block">Create Account</button>
                                                    <p class="mb-0 mt-2">Already have an account?<span data-toggle="modal" data-target="#loginModal" class="red showRegister"> Login</span></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content p-3">
                                <div class="modal-header flex-column pb-0">
                                    <h3 class="modal-title text-center w-100" id="loginModalTitle">Welcome Back</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <p class="w-100 text-center para-text mb-0">CONTINUE WITH</p>
                                </div>
                                <div class="modal-body text-center p-0">
                                    <div class="social-buttons pt-2">
                                        <div class="loginGoogle">
                                            <form action="{{ route('social.login', 'google') }}">
                                                <button class="login-buttons btn" type="submit"><img src="{{ asset('assets/images/google-signin-dark.png') }}" /></button>
                                            </form>
                                        </div>
                                        <div class="loginNext d-flex mt-2">
                                            <form action="{{ route('social.login', 'facebook') }}">
                                                <button type="submit" class="btn btn-default btn-facebook"><i class="fab fa-facebook-square"></i> Facebook</button>
                                            </form>
                                            <form action="{{ route('social.login', 'github') }}">
                                                <button type="submit" class="btn btn-default btn-github"><i class="fab fa-github-square"></i> Github</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="lines d-flex justify-content-center align-items-center pt-3">
                                        <hr class="w-100">
                                        <p class="px-3 m-0">OR</p>
                                        <hr class="w-100">
                                    </div>
                                    <div class="forma p-3">
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text addon"><i class="fas fa-envelope"></i></span>
                                                        </div>
                                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                    </div>
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text addon"><i class="fas fa-lock-alt"></i></span>
                                                        </div>
                                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                                                    </div>
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col">
                                                    <button type="submit" class="btn bg-red btn-block">Login</button>
                                                    <p class="mb-0 mt-2">Don't have an account?<span data-toggle="modal" data-target="#registerModal" class="red showLogin"> Sign Up</span></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="modal fade" id="SubmitTool" tabindex="-1" role="dialog" aria-labelledby="SubmitToolTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content p-3">
                                <div class="modal-header flex-column pb-0">
                                    <h3 class="modal-title text-center w-100" id="SubmitToolTitle">Add New Tool</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body text-center p-0">
                                    <div class="forma p-3">
                                        <div class="errorsmsg alert alert-danger" role="alert"></div>
                                        <div class="successmsg alert alert-success" role="alert"></div>
                                        <form class="createCourse" method="POST" action="">
                                            @csrf
                                            <div class="form-group row text-left ">
                                                <div class="col">
                                                        <label for="name">Course Name</label>
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Course Name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                                </div>
                                            </div>
                                            <div class="form-group row text-left">
                                                <div class="col">
                                                    <label for="link">Link</label>
                                                    <input id="link" type="text" class="form-control @error('link') is-invalid @enderror" name="link" placeholder="Course Link" required autocomplete="link">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col p-0">
                                                    <select class="tools form-control" name="subcategories[]" multiple>
                                                        @foreach(\App\Subcategory::all() as $test)
                                                            <option value="{{ $test->id }}">{{ $test->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col p-0">
                                                    <select class="versionform form-control" name="versions[]" multiple>
                                                        @foreach(\App\Version::all() as $test)
                                                            <option value="{{ $test->id }}">{{ $test->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row text-left">
                                                <div class="col">
                                                    <label for="type">Type</label><br />
                                                    <div class="items d-flex align-items-baseline">
                                                        @foreach(\App\Type::all() as $test)
                                                            <input type="radio" name="type" required class="mx-2" value="{{ $test->id }}"> {{ $test->type }}
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row text-left">
                                                <div class="col">
                                                    <label for="medium">Medium</label><br />
                                                    <div class="items d-flex align-items-baseline">
                                                        @foreach(\App\Medium::all() as $test)
                                                            <input type="radio" name="medium" required class="mx-2" value="{{ $test->id }}"> {{ $test->medium }}
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row text-left">
                                                <div class="col">
                                                    <label for="level">Level</label><br />
                                                    <div class="items d-flex align-items-baseline">
                                                        @foreach(\App\Level::all() as $test)
                                                            <input type="radio" name="level" required class="mx-2" value="{{ $test->id }}"> {{ $test->level }}
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row text-left">
                                                <div class="col">
                                                    <label for="language">Language</label><br />
                                                    <div class="items d-flex align-items-baseline">
                                                        @foreach(\App\Language::all() as $test)
                                                            <input type="radio" name="language" required class="mx-2" value="{{ $test->id }}"> {{ $test->language }}
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col">
                                                    <button type="submit" class="btn bg-red btn-block">Submit Tool</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</nav>
</header>