<footer class="gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Made with <i class="fas fa-heart"></i> from Darko Dimitrovski</p>
            </div>
        </div>
    </div>
</footer>