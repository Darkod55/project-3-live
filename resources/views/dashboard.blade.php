@extends('layouts.master')

@section('page-title')
    Admin Dashboard
@endsection

@section('customscripts')
<script src="{{ asset('assets/js/dashboard.js') }}"></script>
@endsection

@section('content')
<div class="main-container">
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Welcome {{ Auth::user()->name }}</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3 px-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover tablelink">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Medium</th>
                                <th>Level</th>
                                <th>Language</th>
                                <th>User</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="table-append">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection