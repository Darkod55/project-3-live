<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/testfilter', 'PagesController@testfilter')->name('testFilter');
Auth::routes();
Route::get('/dashboard', 'DashboardController@showDashboard')->name('showDashboard')->middleware('checkAdmin');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{slug?}', 'PagesController@index')->name('Index');
Route::get('/courses/{slug?}', 'CoursesController@courses')->name('Courses');


Route::post('/listCourses', 'CoursesController@listCourses')->name('listCourses');
Route::post('/showSubcategories', 'PagesController@showSubcategories')->name('showSubcategories');
Route::post('search', 'PagesController@search');
Route::post('/storeCourse', 'CoursesController@storeCourse')->name('storeCourse');
Route::post('/sortByFunction', 'CoursesController@sortByFunction')->name('sortByFunction');
Route::post('/filterOptions', 'CoursesController@filterOptions')->name('filterOptions');
Route::post('/showInDashboard', 'DashboardController@showInDashboard')->name('showInDashboard');
Route::post('/adminDelete', 'DashboardController@adminDelete')->name('adminDelete');
Route::post('/adminApprove', 'DashboardController@adminApprove')->name('adminApprove');
Route::post('votes/{id}', 'CoursesController@votes');

Route::get('login/{provider}', 'SocialLoginController@redirectToProvider')->name('social.login');
Route::get('login/{provider}/callback', 'SocialLoginController@handleProviderCallback');

