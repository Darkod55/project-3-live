$(document).ready(function () {

        // console.log(AuthUser);
        //Appending Subcategories in Index
        function testFunction(test) {
            $('.courses').append(`
                <div class="item col-md-4 d-flex">
                    <div class="items-in w-100 rounded p-3 my-4">
                        <a class="d-flex align-items-center" href="/courses/${test.slug}">
                            <img src="${test.logo}" />
                            <h6 class="ml-3">${test.name}</h6>
                        </a>
                    </div>
                </div>
            `)
        }

        //Sending CSRF Token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Function for Subcategories
        function showSubcategories() {
            var pathname = window.location.pathname;
            $.post('/showSubcategories', pathname, function(data) {
                // console.log(data);
                data.forEach(value => {
                    if("/" + value.slug == pathname) {
                        value.subcategories.forEach(test => {
                            testFunction(test);
                        })
                    } else if(pathname == "/") {
                        value.subcategories.forEach(test => {
                            testFunction(test);
                        })
                    }
                })
            })
        }
        showSubcategories();
    
        //Search Functionality
        $('#searchbox').on('keyup', function(e) {
            e.preventDefault();
            var searchvalue = $('#searchbox').val();
            // console.log(page_slug);
            // console.log(searchvalue);
            $.post('/search', {
                'search': searchvalue,
                'slug': page_slug
            }, function(data) {
                // console.log(data);
                $('.courses').empty();
                data.forEach(value => {
                    testFunction(value);
                })
            })
        })

        //Login Modal hide/show
        $('#loginModal').on('show.bs.modal', function(){
            $('#registerModal').modal('hide');
        });

        //Register Modal hide/show
        $('#registerModal').on('show.bs.modal', function(){
            $('#loginModal').modal('hide');
        });

        //For Multiselect dropdown in add new course form
        $(".tools").multiselect({   
            nonSelectedText: 'Select Technology',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '434px',
            templates: {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times-circle-o"></i></button></span>',
            }
        });
        $(".versionform").multiselect({   
            nonSelectedText: 'Select Version',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '434px',
            templates: {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times-circle-o"></i></button></span>',
            }
        });

        //For creating new course from header modal
        $('.createCourse').on('submit', function(e) {
            e.preventDefault();
            let objectToSubmit = {
                name: $('#name').val(),
                link: $('#link').val(),
                subcategories: $('.tools').val(),
                versions: $('.versionform').val(),
                type: $('input[type=radio][name=type]:checked').val(),
                medium: $('input[type=radio][name=medium]:checked').val(),
                level: $('input[type=radio][name=level]:checked').val(),
                language: $('input[type=radio][name=language]:checked').val(),
            }
            //console.log(objectToSubmit);
            $.post("/storeCourse", objectToSubmit).then(data => {
                $(".errorsmsg").hide();
                $(".successmsg").show();
                $(".successmsg").append("<li>Course successfully submitted. You will be contacted as soon as your course is approved!</li>");
                $('#name').val('');
                $('#link').val('');
                $('.tools').val('');
                $('.versionform').val('');
                $('#input[type=radio][name=type]').val('');
                $('#input[type=radio][name=medium]').val('');
                $('#input[type=radio][name=level]').val('');
                $('#input[type=radio][name=language]').val('');
            // let test = JSON.parse(data)
            //console.log(data)
            }).catch(function(xhr, status, error) {
                // var errors = err.responseJSON
                $(".errorsmsg").show();
                $.each(xhr.responseJSON.errors, function (key, item) {
                    $(".errorsmsg").append("<li>"+item+"</li>")
                });
                console.log(errors);
            })
        })

        //Stop Tutorial modal if user not logged in
        $('.tutorial-submit').on('click', function() {
            if(!AuthUser) {
                swal({
                    type: 'error',
                    title: 'Please log in to submit a tutorial!',
                    showConfirmButton: true,
                    })
                return
            }
        })
    })