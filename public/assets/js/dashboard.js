$(document).ready(function () {

    //Dashboard append courses
    function appendApproved(value) {
        var approved;
        var dataApprove;
        var textApprove;
        var buttonClass;
        if(value.status == 1) {
            approved = "Approved"
            dataApprove = "notApproved";
            textApprove = "<i class='fas fa-thumbs-down'></i>";
            buttonClass = "btn-info";
        } else if(value.status == 0) {
            approved = "Not Approved"
            dataApprove = "approved";
            textApprove = "<i class='fas fa-thumbs-up'></i>";
            buttonClass = "btn-warning";
        }
        $(".table-append").append(`
            <tr>
                <td>${value.id}</td>
                <td class="name-dashboard"><a href="${value.link}">${value.name}</a></td>
                <td>${value.types['type']}</td>
                <td>${value.mediums['medium']}</td>
                <td>${value.levels['level']}</td>
                <td>${value.languages['language']}</td>
                <td>${value.users['name']}</td>
                <td>${approved}</td>
                <td class="text-center foractions">
                    <a class="approveAction btn ${buttonClass}" data-approve="${dataApprove}" data-id="${value.id}">${textApprove}</a>
                    <a class="adminDelete btn btn-danger" data-id="${value.id}"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
        `)
    }

    //Dashboard show courses
    function showInDashboard() {
        $.post('/showInDashboard', function(data) {
            //console.log(data);
            data.forEach(value => {
                appendApproved(value);
            })
        })
    }
    showInDashboard();

    //Dashboard Delete course
    $('body').on('click', '.adminDelete', function(e) {
        e.preventDefault();
        var course_id = $(this).attr('data-id');
        $('.table-append-approved').empty();
        $('.table-append-unapproved').empty();
        console.log(course_id);
        $.post('/adminDelete', {
            'id': course_id
        }, function(data) {
            console.log(data);
            data.forEach(value => {
                appendApproved(value);
            })
        })
    })

    //Dashboard approve or disapprove course
    $('body').on('click', '.approveAction', function(e) {
        e.preventDefault();
        var course_id = $(this).attr('data-id');
        var approve_action = $(this).attr('data-approve');
        $('.table-append').empty();
        // console.log(approve_action);
        $.post('/adminApprove', {
            'id': course_id,
            'approve_action': approve_action
        }, function(data) {
            console.log(data);
            data.forEach(value => {
                appendApproved(value);
            })
        })
    })
})